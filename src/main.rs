mod config;
mod desktop;
mod error;
mod mode;
mod mpd;
mod pass;
mod ssh;
mod sway;

use desktop::Desktop;
use mode::Mode;
use mpd::Mpd;
use pass::Pass;
use ssh::Ssh;

use anyhow::Result;
use clap::{Parser, Subcommand};
use std::env;

#[derive(Parser, Debug)]
struct Args {
    #[clap(subcommand)]
    command: Command,
}

#[derive(Subcommand, Debug)]
enum Command {
    /// Run an application from installed desktop files
    Desktop,
    /// Open an SSH session using SSH config
    Ssh,
    /// Change the song in the current MPD playlist
    Mpd,
    /// Temporarily copy a password from pass into the clipboard
    Pass,
}

fn main() -> Result<()> {
    let args = Args::parse();

    if let Some(lister) = env::var_os("KAGUYA_LISTER") {
        config::override_lister(lister)?;
    }

    if let Some(args) = env::var_os("KAGUYA_LISTER_ARGS") {
        config::override_lister_args(args)?;
    }

    if let Some(terminal) = env::var_os("KAGUYA_TERMINAL") {
        config::override_terminal(terminal)?;
    }

    match args.command {
        Command::Desktop => Desktop::new().run(),
        Command::Ssh => Ssh::new()?.run(),
        Command::Mpd => Mpd::new()?.run(),
        Command::Pass => Pass::new()?.run(),
    }
}

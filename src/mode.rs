use crate::config;
use crate::error::KaguyaError;

use anyhow::{bail, Result};
use std::ffi::OsStr;
use std::fmt::Display;
use std::io::Write;
use std::os::unix::ffi::OsStrExt;
use std::process::{Command, Stdio};

pub(crate) trait Mode {
    type Item: Display;

    fn list(&self) -> Result<Vec<Self::Item>>;
    fn select(&self, item: &Self::Item) -> Result<()>;

    fn run(&self) -> Result<()> {
        let list = self.list()?;

        let lister = config::lister();
        let args = config::lister_args()
            .as_bytes()
            .split(|byte| *byte == b' ')
            .map(OsStr::from_bytes);

        let mut child = Command::new(lister)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .args(args)
            .spawn()?;

        let mut stdin = child.stdin.take().unwrap();
        for item in &list {
            writeln!(stdin, "{}", item)?;
        }
        drop(stdin);

        let output = child.wait_with_output()?;
        let code = output.status.code();

        if code != Some(0) {
            bail!(match code {
                Some(code) => KaguyaError::ListerFailed(code),
                None => KaguyaError::ListerKilled,
            });
        }

        let selected = String::from_utf8(output.stdout)?;
        let selected = selected.trim();

        let found = list.iter().find(|item| item.to_string() == selected);

        if let Some(item) = found {
            self.select(item)?;
        } else {
            bail!(KaguyaError::ItemNotFound(selected.to_string()));
        }

        Ok(())
    }
}

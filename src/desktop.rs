use crate::config;
use crate::error::DesktopError;
use crate::mode::Mode;
use crate::sway;

use anyhow::{bail, Result};
use std::fmt;
use std::fmt::{Debug, Display, Formatter};
use std::fs;
use std::path::PathBuf;

const DEFAULT_DE_DIRS: &[&str] = &[
    "/usr/share/applications",
    "/var/lib/flatpak/exports/share/applications",
];

pub(crate) struct Entry {
    path: PathBuf,
    entry: freedesktop::Entry,
}

impl Entry {
    pub(crate) fn name(&self) -> &str {
        self.entry
            .section("Desktop Entry")
            .attr("Name")
            .unwrap_or("<<Unnamed Entry>>")
    }

    pub(crate) fn hidden(&self) -> bool {
        self.entry.section("Desktop Entry").attr("NoDisplay") == Some("true")
    }
}

impl Debug for Entry {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        f.write_fmt(format_args!("{}({:?})", self.name(), self.path))
    }
}

impl Display for Entry {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        f.write_fmt(format_args!("{}", self.name()))
    }
}

pub(crate) struct Desktop {
    dirs: Vec<PathBuf>,
}

impl Desktop {
    pub(crate) fn new() -> Self {
        let mut dirs = Vec::new();

        if let Some(home) = dirs::home_dir() {
            let path = home.join(".local/share/applications");

            if path.exists() {
                dirs.push(path);
            }
        }

        for default_dir in DEFAULT_DE_DIRS {
            dirs.push(PathBuf::from(default_dir));
        }

        Desktop { dirs }
    }
}

impl Mode for Desktop {
    type Item = Entry;

    fn list(&self) -> Result<Vec<Self::Item>> {
        let mut paths = Vec::new();

        for dir in &self.dirs {
            if let Ok(read_dir) = fs::read_dir(dir) {
                for dir_entry in read_dir.flatten() {
                    let path = dir_entry.path();

                    if path.extension() == Some("desktop".as_ref()) {
                        paths.push(path);
                    }
                }
            }
        }

        let mut entries = paths
            .into_iter()
            .filter_map(|path| match freedesktop::Entry::parse_file(&path) {
                Ok(entry) => Some(Entry { path, entry }),
                Err(_) => None,
            })
            .filter(|entry| !entry.hidden())
            .collect::<Vec<Entry>>();

        entries.sort_by(|l, r| l.name().cmp(r.name()));
        entries.dedup_by(|l, r| l.path.file_name() == r.path.file_name());

        Ok(entries)
    }

    fn select(&self, item: &Self::Item) -> Result<()> {
        let exec = item.entry.section("Desktop Entry").attr("Exec");

        match exec {
            Some(exec) => {
                let exec = exec.replace("%f", "");
                let exec = exec.replace("%F", "");
                let exec = exec.replace("%u", "");
                let exec = exec.replace("%U", "");

                if exec.is_empty() {
                    bail!(DesktopError::EmptyExec);
                }

                if item.entry.section("Desktop Entry").attr("Terminal") == Some("true") {
                    let mut term = config::terminal().to_owned();
                    term.push(" -e ");
                    term.push(&exec);

                    sway::msg_exec(term)?;
                } else {
                    sway::msg_exec(exec)?;
                }
            }
            None => bail!(DesktopError::MissingExec),
        }

        Ok(())
    }
}

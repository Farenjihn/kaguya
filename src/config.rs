use anyhow::{Context, Result};
use once_cell::sync::OnceCell;
use std::ffi::{OsStr, OsString};

const DEFAULT_LISTER: &str = "fzf";
const DEFAULT_LISTER_ARGS: &str = "--padding=5%";
const DEFAULT_TERMINAL: &str = "alacritty";

static LISTER: OnceCell<OsString> = OnceCell::new();
static LISTER_ARGS: OnceCell<OsString> = OnceCell::new();
static TERMINAL: OnceCell<OsString> = OnceCell::new();

pub(crate) fn lister() -> &'static OsStr {
    LISTER.get_or_init(|| DEFAULT_LISTER.into())
}

pub(crate) fn override_lister(lister: OsString) -> Result<()> {
    LISTER
        .set(lister)
        .ok()
        .context("failed to override lister")?;

    Ok(())
}

pub(crate) fn lister_args() -> &'static OsStr {
    LISTER_ARGS.get_or_init(|| DEFAULT_LISTER_ARGS.into())
}

pub(crate) fn override_lister_args(args: OsString) -> Result<()> {
    LISTER_ARGS
        .set(args)
        .ok()
        .context("failed to override lister args")?;

    Ok(())
}

pub(crate) fn terminal() -> &'static OsStr {
    TERMINAL.get_or_init(|| DEFAULT_TERMINAL.into())
}

pub(crate) fn override_terminal(terminal: OsString) -> Result<()> {
    TERMINAL
        .set(terminal)
        .ok()
        .context("failed to override terminal")?;

    Ok(())
}

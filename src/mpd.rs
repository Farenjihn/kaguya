use crate::mode::Mode;

use anyhow::Result;
use mpd_client::commands::definitions::*;
use mpd_client::commands::SongPosition;
use mpd_client::Client;
use std::fmt;
use std::fmt::{Debug, Display, Formatter};
use tokio::net::UnixStream;
use tokio::runtime::{Builder, Runtime};

#[derive(Debug)]
pub(crate) struct SongItem {
    position: SongPosition,
    title: String,
    artist: String,
}

impl Display for SongItem {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        f.write_fmt(format_args!(
            "{}: {} - {}",
            self.position.0, self.title, self.artist
        ))
    }
}

pub(crate) struct Mpd {
    runtime: Runtime,
}

impl Mpd {
    pub(crate) fn new() -> Result<Self> {
        let runtime = Builder::new_current_thread().enable_all().build()?;
        Ok(Mpd { runtime })
    }
}

impl Mode for Mpd {
    type Item = SongItem;

    fn list(&self) -> Result<Vec<Self::Item>> {
        self.runtime.block_on(async {
            let client = connect().await?;

            let songs = client
                .command(Queue)
                .await?
                .iter()
                .map(|queued| {
                    let title = queued.song.title().unwrap_or("no title").into();
                    let artist = queued.song.artists().join(",");

                    SongItem {
                        position: queued.position,
                        title,
                        artist,
                    }
                })
                .collect();

            Ok(songs)
        })
    }

    fn select(&self, item: &Self::Item) -> Result<()> {
        self.runtime.block_on(async {
            let client = connect().await?;
            client.command(Play::song(item.position)).await?;

            Ok(())
        })
    }
}

async fn connect() -> Result<Client> {
    let euid = unsafe { libc::geteuid() };

    let socket = format!("/run/user/{}/mpd/socket", euid);
    let stream = UnixStream::connect(socket).await?;

    let (client, _) = Client::connect(stream).await?;

    Ok(client)
}

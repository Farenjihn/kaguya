#[derive(thiserror::Error, Debug)]
pub(crate) enum KaguyaError {
    #[error("swaymsg exited with code = {0}")]
    SwaymsgFailed(i32),
    #[error("swaymsg was killed")]
    SwaymsgKilled,
    #[error("finder exited with code = {0}")]
    ListerFailed(i32),
    #[error("finder was killed")]
    ListerKilled,
    #[error("could not find item in list: '{0}'")]
    ItemNotFound(String),
}

#[derive(thiserror::Error, Debug)]
pub(crate) enum DesktopError {
    #[error("desktop entry is missing an Exec attribute")]
    MissingExec,
    #[error("desktop entry has an empty Exec attribute")]
    EmptyExec,
}

#[derive(thiserror::Error, Debug)]
pub(crate) enum PassError {
    #[error("pass exited with code = {0}")]
    PassFailed(i32),
    #[error("pass was killed")]
    PassKilled,
    #[error("wl-copy exited with code = {0}")]
    WlCopyFailed(i32),
    #[error("wl-copy was killed")]
    WlCopyKilled,
}

use crate::error::PassError;
use crate::mode::Mode;

use anyhow::{bail, Context, Result};
use std::io::Write;
use std::path::PathBuf;
use std::process::{Command, Stdio};
use walkdir::{DirEntry, WalkDir};

pub(crate) struct Pass {
    store: PathBuf,
}

impl Pass {
    pub(crate) fn new() -> Result<Self> {
        let home = dirs::home_dir().context("could not find HOME directory")?;
        let store = home.join(".password-store");

        Ok(Pass { store })
    }
}

impl Mode for Pass {
    type Item = String;

    fn list(&self) -> Result<Vec<Self::Item>> {
        let mut passwords = Vec::new();

        for entry in WalkDir::new(&self.store)
            .into_iter()
            .filter_entry(|entry| !is_hidden(entry))
        {
            let entry = entry?;

            if !entry.file_type().is_file() {
                continue;
            }

            let path = entry.into_path();
            let path = path.strip_prefix(&self.store)?;
            let path = path.to_str().context("password name is not valid UTF-8")?;

            if let Some(path) = path.strip_suffix(".gpg") {
                passwords.push(path.to_string());
            }
        }

        Ok(passwords)
    }

    fn select(&self, item: &Self::Item) -> Result<()> {
        let output = Command::new("pass")
            .arg(item)
            .stdout(Stdio::piped())
            .output()?;

        let code = output.status.code();
        if code != Some(0) {
            bail!(match code {
                Some(code) => PassError::PassFailed(code),
                None => PassError::PassKilled,
            });
        }

        let mut child = Command::new("wl-copy")
            .arg("-o")
            .stdin(Stdio::piped())
            .spawn()?;

        child.stdin.take().unwrap().write_all(&output.stdout)?;

        let output = child.wait_with_output()?;
        let code = output.status.code();

        if code != Some(0) {
            bail!(match code {
                Some(code) => PassError::WlCopyFailed(code),
                None => PassError::WlCopyKilled,
            });
        }

        Ok(())
    }
}

fn is_hidden(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| s != "." && s != ".." && s != ".password-store" && s.starts_with('.'))
        .unwrap_or(false)
}

use crate::config;
use crate::mode::Mode;
use crate::sway;

use anyhow::{Context, Result};
use confindent::Confindent;
use std::path::PathBuf;

pub(crate) struct Ssh {
    config: PathBuf,
}

impl Ssh {
    pub(crate) fn new() -> Result<Self> {
        let home = dirs::home_dir().context("could not find HOME directory")?;
        let config = home.join(".ssh/config");

        Ok(Ssh { config })
    }
}

impl Mode for Ssh {
    type Item = String;

    fn list(&self) -> Result<Vec<Self::Item>> {
        let config = Confindent::from_file(&self.config)?;
        let hosts = config
            .children("Host")
            .iter()
            .filter_map(|value| value.value().map(ToString::to_string))
            .collect();

        Ok(hosts)
    }

    fn select(&self, item: &Self::Item) -> Result<()> {
        let mut term = config::terminal().to_owned();
        term.push(" --title ");
        term.push(item);
        term.push(" -e ssh ");
        term.push(item);

        sway::msg_exec(term)?;

        Ok(())
    }
}

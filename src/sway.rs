use crate::error::KaguyaError;

use anyhow::{bail, Result};
use std::ffi::OsStr;
use std::process::Command;

pub(crate) fn msg_exec<T>(exec: T) -> Result<()>
where
    T: AsRef<OsStr>,
{
    let mut command = Command::new("swaymsg");
    command.arg("exec");
    command.arg(exec);

    let code = command.spawn()?.wait()?.code();
    if code != Some(0) {
        bail!(match code {
            Some(code) => KaguyaError::SwaymsgFailed(code),
            None => KaguyaError::SwaymsgKilled,
        });
    }

    Ok(())
}

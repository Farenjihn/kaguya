# Kaguya

A hacky "list and do the thing" program for Sway. Do not expect this to be high quality, I made it mostly to have some fun.

## Features

- List desktop entries and run the selected entry.
- List SSH configurations in `~/.config/ssh` and opens up a session for the selected entry.
- List the contents of the current MPD playlist (queue) and play the selected entry.
- List passwords (`pass`) and copy the selected entry to the wayland clipboard.

## Defaults

By default, `kaguya` uses `fzf` and `alacritty` as entry lister and terminal to run terminal applications or open SSH sessions. Both of those can be configured via the `KAGUYA_LISTER`, `KAGUYA_LISTER_ARGS` and `KAGUYA_TERMINAL` environment variables.

The lister is expected to read the choice list from stdin and output the selected entry in stdout. The terminal emulator is expected to provide a `-e` argument to allow running a provided program with its arguments.

## Dependencies

On top of the above, `kaguya` uses `wl-copy` as an external program to copy the selected password in password mode to the wayland clipboard.

## Installation

Make sure you have the required runtime dependencies (optional depending on your setup):

```bash
sudo pacman -S sway fzf alacritty wl-clipboard
```

Then build it using `cargo install --path . -f`.

## Simple Guide

You can, for instance, add the following to your sway configuration:

```
for_window [app_id="^kaguya$"] floating enable, move position center, resize set 600 375

mode "listing" {
    bindsym $mod+s exec $term --class kaguya -e kaguya ssh; mode "default"
    bindsym $mod+m exec $term --class kaguya -e kaguya mpd; mode "default"
    bindsym $mod+p exec $term --class kaguya -e kaguya pass; mode "default"

    bindsym Return mode "default"
    bindsym Escape mode "default"
}

bindsym $mod+l mode "listing"
bindsym $mod+d exec $term --class kaguya -e kaguya desktop
```

This will provide a listing menu to allow using the various modes `kaguya` provides, as well as configure sway to center shell running kaguya.
